FROM ubuntu:18.04
ARG username=restAPI_user
ARG userid=10007
RUN echo "building image with username: ${username}, userid: ${userid}"
# Install system packages
RUN apt-get update && apt-get install -y --no-install-recommends \
      bzip2 \
      g++ \
      git \
      graphviz \
      libgl1-mesa-glx \
      iputils-ping \
      curl \
      libhdf5-dev \
      vim \
      openmpi-bin \
      wget && \
    rm -rf /var/lib/apt/lists/*

RUN apt-get update

RUN apt-get install -y libgtk2.0-dev

# install poppler utils for pdf2image
RUN apt-get install -y poppler-utils
# Install miniconda
ENV CONDA_DIR /opt/conda
ENV PATH $CONDA_DIR/bin:$PATH

RUN wget --quiet --no-check-certificate https://repo.continuum.io/miniconda/Miniconda3-4.2.12-Linux-x86_64.sh && \
    echo "c59b3dd3cad550ac7596e0d599b91e75d88826db132e4146030ef471bb434e9a *Miniconda3-4.2.12-Linux-x86_64.sh" | sha256sum -c - && \
    /bin/bash /Miniconda3-4.2.12-Linux-x86_64.sh -f -b -p $CONDA_DIR && \
    rm Miniconda3-4.2.12-Linux-x86_64.sh && \
    echo export PATH=$CONDA_DIR/bin:'$PATH' > /etc/profile.d/conda.sh

# create user, add home directory and change ownership of conda directory
RUN useradd -u ${userid} -m -s /bin/bash -N ${username}
RUN chown ${username} $CONDA_DIR -R
# for the user to access the config file
USER ${username}
RUN mkdir /home/${username}/restAPI_deployment


ARG python_version=3.6
USER root
RUN conda config --set ssl_verify false
RUN conda install -y python=${python_version}
# RUN pip install --upgrade pip
RUN pip install --trusted-host pypi.python.org --upgrade pip

# poppler is required for pdftoimage
COPY requirements.txt /home/${username}/restAPI_deployment/requirements.txt
# RUN pip install -r /home/${username}/restAPI_deployment/requirements.txt
RUN pip install --trusted-host pypi.org --trusted-host files.pythonhosted.org pip -r /home/${username}/restAPI_deployment/requirements.txt
RUN rm /home/${username}/restAPI_deployment/requirements.txt

USER ${username}
# copy the restAPI_main code
COPY main_app /home/${username}/restAPI_deployment/main_app
# create a log directory
RUN mkdir /home/${username}/restAPI_deployment/restAPI_log_dir
RUN mkdir /home/${username}/restAPI_deployment/binaries
COPY binaries/* /home/${username}/restAPI_deployment/binaries/
# Use double quotes and not single quotes
RUN echo "export RESTAPI_LOG_DIR=/home/${username}/restAPI_deployment/restAPI_log_dir" >> ~/.bashrc
RUN echo "export PYTHONPATH=$PYTHONPATH:/home/${username}/restAPI_deployment" >> ~/.bashrc
COPY server /home/${username}/restAPI_deployment/server

WORKDIR /home/${username}/restAPI_deployment/server

EXPOSE 8000 8001
