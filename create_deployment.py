import main_app
import subprocess
version = main_app.__version__

image_name = "diy_reset_api_boilerplate"
print("--------------------building docker image with the following name: {}:{}------------------------------".format(image_name, version))
subprocess.call(["docker", "build", "--no-cache", "-t", "{}:{}".format(image_name, version), "."])

print("--------------------Manually push the image to docker hub! Remember to sign in to dockerhub before you push!-----------------------")
