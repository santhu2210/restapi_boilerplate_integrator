import logging
import os
import flask
import json
from time import time
from main_app import MainAppEngine
from main_app import __version__ as version

app = flask.Flask(__name__)

# Logger Config
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter("%(asctime)s:%(levelname)s:%(name)s:%(message)s")
filehandler = logging.FileHandler(os.path.join(os.environ["RESTAPI_LOG_DIR"], "restAPI_flask_app_v{}.log".format(version)))
filehandler.setFormatter(formatter)

logger.addHandler(filehandler)

# setup the MainAPP Engine
# config_file = "/home/restAPI_user/restAPI_deployment/server/config.json"  # path inside docker container

present_config_file = os.path.dirname(os.path.abspath(__file__))
config_file = os.path.join(present_config_file, "config.json")

with open(config_file, "r") as f:
    loaded_config = json.load(f)

logger.info("Initializing the Main APP engine...")
engine = MainAppEngine(loaded_config)
logger.info("RESTAPI Server is ready!")

# -------------------------------------------------------------------------------- #


@app.route("/")
@app.route("/home")
def home():
    return "RestAPI Boilerplate Application. Customize this asper your requirement. The /predict for POST requests. The payload should be {'username':username}"


@app.route("/predict", methods=["POST"])
def predict():
    data = {"status": 500}
    if flask.request.method == "POST":
        st_time = time()
        try:
            logger.info("Input received is {}".format(flask.request.get_json()))
            req_data = flask.request.get_json()
            username = req_data['username']

        except Exception:
            message = "Error in Input."
            logger.exception(message)
            data["status"] = 400
            data["exception"] = message
            return flask.jsonify(data)

        logger.info("time taken to get data and read for user is {}".format(time() - st_time))
        st_time = time()
        try:
            predict_dict = engine.predict(username=username)

        except Exception as e:
            message = "Unknown error: {} . See logs for details".format(e)
            logger.exception(message)
            data["status"] = 500
            data["exception"] = message
            return flask.jsonify(data)

        logger.info("Time taken for prediction call is {}".format(time() - st_time))
        data["status"] = 200
        data["data"] = predict_dict

    return flask.jsonify(data)
