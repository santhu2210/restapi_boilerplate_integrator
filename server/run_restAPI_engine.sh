#!/bin/bash
# TODO: Move this to Dockerfile
export RESTAPI_LOG_DIR=/home/restAPI_user/restAPI_deployment/restAPI_log_dir
export PYTHONPATH=$PYTHONPATH:/home/restAPI_user/restAPI_deployment
gunicorn -t 1500 -w 5 --bind 0.0.0.0:8000 wsgi:app
# nohup python wsgi.py &

# export RESTAPI_LOG_DIR=/home/shantakumar/Projects/Developement_projects/restAPI_BP_app/log_dir
# export PYTHONPATH=$PYTHONPATH:/home/shantakumar/Projects/Developement_projects/restAPI_BP_app/reset_api_boilerplate
