"""
Run this script directly from terminal for running flask server in debug mode.
Do not use this in production
"""
from restapi_server import app

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=8000)
