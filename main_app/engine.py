import logging
import os
import json

from .about import __version__ as version

# Logger Config
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter("%(asctime)s:%(levelname)s:%(name)s:%(message)s")
filehandler = logging.FileHandler(os.path.join(os.environ["RESTAPI_LOG_DIR"], "mainapp_engine_v{}.log".format(version)))
filehandler.setFormatter(formatter)

logger.addHandler(filehandler)


class MainAppEngine:
    def __init__(self, config):
        logger.info("Initializing the engine")
        self.config = config
        self.note = "Welcome "
        self.cnt = 0
        logger.info("Engine is ready for predictions")

    def predict(self, username):

        self.username = username
        self.cnt = self.cnt + 1

        predict_dict = {"User": self.username,
                        "Note": self.note + self.username,
                        "Hit_count": self.cnt}

        # predict_string = json.dumps(predict_dict)

        logger.info("Prediction completed..")

        return predict_dict
